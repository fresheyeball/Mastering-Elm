module Msg exposing (..)

import Time


type Msg
    = Play
    | Pause
    | StopEject
    | Rewind
    | FastForward
    | Record
    | Insert
    | Tick Time.Time
