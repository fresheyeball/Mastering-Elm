module Update exposing (..)

import Msg
import Model
import Audio


update : Msg.Msg -> Model.Model -> ( Model.Model, Cmd Msg.Msg )
update msg ({ status } as model) =
    case ( msg, status ) of
        ( Msg.Insert, Model.Ejected ) ->
            ( { model | status = Model.Stopped }, Audio.clickHard )

        ( _, Model.Ejected ) ->
            ( model, Cmd.none )

        ( Msg.Play, _ ) ->
            { model | status = Model.Playing } ! [ Audio.play, Audio.clickSoft ]

        ( Msg.Pause, Model.Stopped ) ->
            ( model, Cmd.none )

        ( Msg.Pause, Model.Paused ) ->
            update Msg.Play model

        ( Msg.Pause, _ ) ->
            { model | status = Model.Paused } ! [ Audio.stop, Audio.clickSoft ]

        ( Msg.StopEject, Model.Stopped ) ->
            ( { model | status = Model.Ejected }, Audio.clickHard )

        ( Msg.StopEject, _ ) ->
            { model | status = Model.Stopped } ! [ Audio.stop, Audio.clickHard ]

        ( Msg.Rewind, _ ) ->
            { model | status = Model.Rewinding } ! [ Audio.stop, Audio.clickSoft ]

        ( Msg.FastForward, _ ) ->
            { model | status = Model.FastForwarding } ! [ Audio.stop, Audio.clickSoft ]

        ( Msg.Tick delta, Model.Rewinding ) ->
            let
                delta_ =
                    0 - delta * 2
            in
                ( { model | playhead = model.playhead + delta_ }, Audio.seek delta_ )

        ( Msg.Tick delta, Model.FastForwarding ) ->
            let
                delta_ =
                    delta * 2
            in
                ( { model | playhead = model.playhead + delta_ }, Audio.seek delta_ )

        ( Msg.Tick delta, Model.Playing ) ->
            ( { model | playhead = model.playhead + delta }, Cmd.none )

        _ ->
            ( model, Cmd.none )
